# yoga - a python REST endpoint for sending emails to Mailgun and Mandrill

**Summary**: Does what it says on the tin. I named it "yoga" because that's the Janelle Monae song I was listening to when I wrote it and it makes about as much sense as most project names. ¯\\\_(ツ)_/¯

It uses REST for reciept and forward to the Mailgun and Mandrill mail services, so neither vendor's libraries are used.

**Tech used**:

* Python 3.6 - Because I know Python better than Node and I wanted this to be more lightweight than what a Node.js project would end up being. Go was also an option, but I'm just now learning it.
* Flask - For flexibility and to prevent the nightmare of management sprawl that can occur from creating seperate Lambdas for each REST method.
* Serverless Framework (SLS) - This was ultimately why I went against using RoR. This kind of service screams "serverless". Using a serverless model, there's obviously the benefit of not having "infrastructure" to deal with, but it will also be incredibly cheap to run, dollars a month for millions of transactions.
    * AWS Lambda 
    * AWS API Gateway 

## How to Deploy

### Local Setup

* Install Python 3.6 - If you're on a Mac, it's just `$ brew install python3`
* Install the [Serverless Framework](https://serverless.com/framework/docs/providers/aws/guide/quick-start/)
* Configure your [AWS creds](https://serverless.com/framework/docs/providers/aws/guide/credentials/) - This setup assumes you are using profiles in your local AWS creds file.
* Clone the repo and cd into the /yoga directory.
* Install SLS' required plugins with `$ npm install`
* Source the included virtualenv just to make things clean:
    *  `$ pip3 install virtualenv` - if you don't have it already installed.
    * `$ virtualenv venv --python=python3`
    *  `$ source venv/bin/activate`
    * `$ pip3 install -r requirements.txt`

### Running the deploy

SLS has been configured to support multiple environments by passing options to the CLI. **Example**: `$ sls deploy --stage=dev` will deploy using dev settings and the dev AWS profile in your creds. Dev stage is the default. See serverless.yml file for details. 

#### Additional variables

* --mail_service (optional, values: mailgun, mandrill) Specifies which mail service the API will use (mailgun or mandrill). The default is mailgun.
* -- domain_name (required, values: example.com) Domain you will be sending mail from. Required for mailgun.
* -- mailgun_key (required) Your mailgun API key.
* -- mandrill_key (required) Your mandrill API key. 

**Example**: `$ sls deploy --stage=prod --mail_service=mailgun --domain_name=example.com --mailgun_key=KEY --mandrill_key=KEY`  

## Running the API

Post deploy, SLS will return an url with a /email endpoint.

Send a POST to this endpoint that includes values for:

* to
* to_name
* from
* from_name
* subject
* body

**Example**: `$ curl -X POST -H "Content-Type: application/json" -d '{"to":"chris@example.com","to_name":"Chris Dodds","from":"dave@example.com","from_name":"Chris","subject":"TEST EMAIL","body":"<html><p>hello, world!</p></html>"}' https://SLS_RETURNED_URL/dev/email`

If the message is successfully queued to send, you'll recieve:

`{"message": "Success!"}` and a 200 response

Otherwise, you'll get a 400 with the error detail. Details for each transaction are logged in AWS Cloudtrail.

### Tests

Tests use py.test. Use `$pip3 install -U pytest` to install and `$ py.test tests.py` to run.

You can run an instance of the API locally by running `$ sls wgsi serve`

## Future improvements / other considerations

* Enhance security & recoverability. SLS cannot deploy AWS KMS or SQS on its own, so another tool like Terraform could be used to deploy a KMS key and SQS DLQ for use by the Lambda function to 1.) encrypt the Lambda env-vars and 2.) add DLQ functionality. Related, moving the API keys to AWS's secure store would also be better than passing them in the CLI. In the meantime, they can be passed as local env-vars. 
* Add test coverage. I honestly haven't written unit tests for Flask before and ran into issues getting response stubs working with py.test so I made the call to punt and proceed without coverage there. As part of a CI/CD pipeline I would mitigate this somewhat with integration tests. I'd also learn how to run mocks/stubs with py.test. :D
* Add send validation. Right now, the API only confirms that the message is queued on the remote service. If it queues and then fails, the API doesn't know about it.
* General thoughts. I probably bit off too many "new to me" things for this. It was actually the first thing I've written using SLS, having mostly used Ansible for prior serverless setups. Between having to research some non-obvious SLS items, troubleshoot py.test issues, and deal with some suprise (not bad) family stuff, I started to run out of time.
