import os, re, cgi, json, requests

from flask import Flask, jsonify, request

app = Flask(__name__)

mailService = os.environ.get('MAIL_SERVICE')
mandrillKey = os.environ.get('MANDRILL_KEY')
mailgunKey = os.environ.get('MAILGUN_KEY')
domainName = os.environ.get('DOMAIN_NAME')

@app.route('/email', methods=['POST'])
def createEmail():
    print('Parsing incoming data.')
    try:
        toAddress = request.json.get('to')
        toName = request.json.get('to_name')
        fromAddress = request.json.get('from')
        fromName = request.json.get('from_name')
        subject = request.json.get('subject')
        body = request.json.get('body'),
        plainTextBody = htmlToText(request.json.get('body'))

        if not all((toAddress, toName, fromAddress, fromName, subject, body)):
            return jsonify({'error': 'Please provide to, to_name, from, from_name, subject, & body'}), 400

        emailRegex = re.compile(r"[^@]+@[^@]+\.[^@]+")

        if not emailRegex.match(toAddress) or not emailRegex.match(fromAddress):
            return jsonify({'error': 'A supplied email address is invalid.'}), 400

        payload = {
            'to': toAddress,
            'to_name': toName,
            'from': fromAddress,
            'from_name': fromName,
            'subject': subject,
            'body': body,
            'plain_text_body': plainTextBody
        }

        print("Incoming data parsed.")
        sendMail(payload)

    except (SystemError, RuntimeError):
        return jsonify({'error': 'Failed to send mail!'}), 400
    else:
        return jsonify({'message': 'Success!'}), 200        
           

def htmlToText(body):
    tagRegex = re.compile(r'(<!--.*?-->|<[^>]*>)')
    scrubbedBody = tagRegex.sub('', body)
    plainText = cgi.escape(scrubbedBody)
    return plainText

def sendMail(payload):
    if mailService != 'mailgun' or mailService != 'mandrill':
        if mailService == 'mailgun':
            return mailgunSend(payload)
        elif mailService == 'mandrill':
            return mandrillSend(payload)
    else:
        print('Error: A mail service is not defined or is mis-configured. Check config.')
        return jsonify({'error': 'A mail service is not defined or is mis-configured. Check config.'}), 400       
    

def mailgunSend(payload):
    key = ('api', mailgunKey)
    messageData = {'from': payload['from_name'] + '<' + payload['from'] +'>',
        'to': payload['to_name'] + '<' + payload['to'] +'>',
        'subject': payload['subject'],
        'html': payload['body'],
        'text': payload['plain_text_body']
        }
    try:    
        message = requests.post('https://api.mailgun.net/v3/' + domainName + '/messages',data=messageData,auth=key)
        message.raise_for_status()
        if "Queued" not in message.text:
            raise messageNotQueued
    except messageNotQueued:
        print('Mailgun did not queue the email. Check Mailgun logs.')
        print(message.text)
        return jsonify({'error': 'Mailgun did not queue the email. Check Mailgun logs.'}), 400   
    except requests.exceptions.HTTPError as err:
        print(err)       
        return jsonify({'error': err }), 400
    except requests.exceptions.Timeout:
        print('Request to Mailgun timed out.')
        return jsonify({'error': err }), 400
    else:
        print(message.text)
        print('Message successfully queued on Mailgun.')   
        # Workaround to Flask null return.
        return ''
        

def mandrillSend(payload):
    url = 'https://mandrillapp.com/api/1.0/messages/send.json'
    messageData = {'key': mandrillKey,
            'message': {
                'html': payload['body'],
                'text': payload['plain_text_body'],                
                'subject': payload['subject'],
                'from_email': payload['from'],                                    
                'from_name': payload['from_name'],
                'to': [{
                    'email': payload['to'],
                    'name': payload['to_name'],
                    'type': 'to'}]
            }
    }
    try:
        message = requests.post(url,json=messageData)
        message.raise_for_status()
        if 'error' in message.text:
            raise messageNotQueued  
    except messageNotQueued:
        print('Mandrill did not queue the email. Check Mandrill logs.')
        print(message.text)
        return jsonify({'error': 'Mandrill did not queue the email. Check Mandrill logs.'}), 400       
    except requests.exceptions.HTTPError as err:
        print(err)          
        return jsonify({'error': err }), 400
    except requests.exceptions.Timeout:
        print('Request to Mandrill timed out.')
        return jsonify({'error': err }), 400      
    else:
        print(message.text) 
        print('Message successfully queued on Mandrill.') 
        # Workaround to Flask null return.
        return ''    

class messageNotQueued(Exception):
    """Raised when the message has not been queued."""
    pass